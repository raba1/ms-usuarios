package com.usuarios.ms.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.usuarios.ms.dto.UsuarioRequestDto;
import com.usuarios.ms.dto.UsuarioResponseDto;
import com.usuarios.ms.service.UsuarioService;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@Value("${jwt.header}")
    private String tokenHeader;

	@PostMapping("/save")
	public ResponseEntity<UsuarioResponseDto> save(@RequestBody UsuarioRequestDto request, HttpServletRequest httpServletRequest) {
		try {
			String token = httpServletRequest.getHeader(tokenHeader);
			return new ResponseEntity<>(usuarioService.save(request, token), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
