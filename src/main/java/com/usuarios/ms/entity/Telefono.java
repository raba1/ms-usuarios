package com.usuarios.ms.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Table(name = "tbl_Telefono")
@Entity
@Data
@JsonIgnoreProperties({ "usuario" })
public class Telefono implements Serializable {

	private static final long serialVersionUID = -72314376457302010L;

	@Id
	@Column(name = "ID_TELEFONO", unique = true, nullable = false)
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	private UUID id;

	@ManyToOne()
	@JoinColumn(name = "ID_USUARIO")
	private Usuario usuario;

	private String number;
	private String citycode;
	private String countrycode;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getCitycode() {
		return citycode;
	}

	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public Telefono() {
		super();
	}

	public Telefono(Usuario usuario, String number, String citycode, String countrycode) {
		this.usuario = usuario;
		this.number = number;
		this.citycode = citycode;
		this.countrycode = countrycode;
	}

}
