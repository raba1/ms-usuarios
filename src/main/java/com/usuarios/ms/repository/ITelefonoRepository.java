package com.usuarios.ms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.usuarios.ms.entity.Telefono;

@Repository
public interface ITelefonoRepository extends JpaRepository<Telefono, Long>{

}
