package com.usuarios.ms.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.usuarios.ms.entity.Usuario;

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long>{
	
	@Query(value = "SELECT * FROM TBL_USUARIO u WHERE u.ID_USUARIO = ?1", nativeQuery = true)
	Usuario findByUUID(UUID id);
	
}
