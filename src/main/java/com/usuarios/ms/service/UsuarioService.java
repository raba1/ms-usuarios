package com.usuarios.ms.service;

import com.usuarios.ms.dto.UsuarioRequestDto;
import com.usuarios.ms.dto.UsuarioResponseDto;

public interface UsuarioService {
	
	public UsuarioResponseDto save(UsuarioRequestDto request, String token);
	
}
