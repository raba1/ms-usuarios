package com.usuarios.ms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.usuarios.ms.config.JwtTokenUtil;
import com.usuarios.ms.dto.TelefonoDto;
import com.usuarios.ms.dto.UsuarioRequestDto;
import com.usuarios.ms.dto.UsuarioResponseDto;
import com.usuarios.ms.entity.Telefono;
import com.usuarios.ms.entity.Usuario;
import com.usuarios.ms.repository.IUsuarioRepository;
import com.usuarios.ms.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{
	
	@Autowired
	private IUsuarioRepository iUsuarioRepository;
	
	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Value("${exp-regular-formato-correo}")
	private String expRegularCorreo;
	
	@Value("${exp-regular-password}")
	private String expRegularPassWord;
	
	@Value("${msj-correo-ya-registrado}")
	private String correoYaRegistrado;
	
	@Value("${msj-formato-correo-invalido}")
	private String formatoCorreoInvalido;
	
	@Value("${msj-formato-password-invalido}")
	private String formatoPasswordInvalido;
	
	public UsuarioResponseDto save(UsuarioRequestDto request, String token) {
		
		UsuarioResponseDto response = new UsuarioResponseDto();
		
		if (!this.validaFormatoDatosEntrada(request, response)) {
			return response;
		}
		
		if (this.validaExistenciaCorreo(request.getEmail())) {
			response.setMensaje(correoYaRegistrado);
			return response;
		}
		
		//GENERO EL DTO PARA GUARDAR LOS DATOS
		Usuario usr = new Usuario();
		usr.setEmail(request.getEmail());
		usr.setName(request.getName());
		usr.setPassword(request.getPassword());
		
		for (TelefonoDto item : request.getPhones()) {
			Telefono tel = new Telefono();
			tel.setCitycode(item.getCitycode());
			tel.setCountrycode(item.getCountrycode());
			tel.setNumber(item.getNumber());
			usr.addTelefono(tel);
		}
		
		boolean isActive = false;
		String tokenSinBearer = token.replaceAll("Bearer ", "");
		
		if (!jwtTokenUtil.isTokenExpired(tokenSinBearer)) {
			isActive = true;
		}
		
		//LLAMO AL METODO PARA GUARDAR
		Usuario usrGuardado = iUsuarioRepository.save(usr);
		if (usrGuardado != null) {
			response.setCreated(usrGuardado.getCreated());
			response.setId(usrGuardado.getId());
			response.setModified(usrGuardado.getModified());
			response.setLastLogin(usrGuardado.getLastLogin());
			response.setMensaje(null);
			response.setToken(tokenSinBearer);
			response.setActive(isActive);
			response.setUsuario(usrGuardado);
		}
		
		return response;
		
	}
	
	public boolean validaFormatoDatosEntrada(UsuarioRequestDto request, UsuarioResponseDto response) {
		boolean isValido = true;
		
		if(!this.validaStringExpRegular(request.getEmail(), expRegularCorreo)) {
			response.setMensaje(formatoCorreoInvalido);
			isValido = false;
			return isValido;
		}
		
		if(!this.validaStringExpRegular(request.getPassword() , expRegularPassWord)) {
			response.setMensaje(formatoPasswordInvalido);
			isValido = false;
			return isValido;
		}
		
		return isValido;
		
	}
	
	public boolean validaExistenciaCorreo(String correo) {
		List<Usuario> listaUsuarios = iUsuarioRepository.findAll();
		return listaUsuarios.stream().filter(p -> correo.equalsIgnoreCase(p.getEmail())).findFirst().isPresent();
	}
	
	public boolean validaStringExpRegular(String str, String expRegular) {
		return str.matches(expRegular);
	}

}
