drop table if exists tbl_telefono CASCADE;
drop table if exists tbl_usuario CASCADE; 
create table tbl_telefono (id_telefono binary not null, citycode varchar(255), countrycode varchar(255), number varchar(255), id_usuario binary, primary key (id_telefono));
create table tbl_usuario (id_usuario binary not null, created timestamp, email varchar(255), last_login timestamp, modified timestamp, name varchar(255), password varchar(255), primary key (id_usuario));
alter table tbl_telefono add constraint FK foreign key (id_usuario) references tbl_usuario;