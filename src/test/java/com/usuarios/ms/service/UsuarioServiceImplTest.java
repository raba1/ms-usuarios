package com.usuarios.ms.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.usuarios.ms.config.JwtTokenUtil;
import com.usuarios.ms.dto.TelefonoDto;
import com.usuarios.ms.dto.UsuarioRequestDto;
import com.usuarios.ms.dto.UsuarioResponseDto;
import com.usuarios.ms.entity.Telefono;
import com.usuarios.ms.entity.Usuario;
import com.usuarios.ms.repository.IUsuarioRepository;
import com.usuarios.ms.service.impl.UsuarioServiceImpl;

@ExtendWith(MockitoExtension.class)
public class UsuarioServiceImplTest {
	
	@InjectMocks
	private UsuarioServiceImpl usuarioServiceImpl;
	
	@Mock
	private IUsuarioRepository iUsuarioRepository;
	
	@Mock
	private JwtTokenUtil jwtTokenUtil;
	
	@Test
	public void saveTest() throws ParseException {
		
		ReflectionTestUtils.setField(usuarioServiceImpl,"expRegularCorreo","^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
		ReflectionTestUtils.setField(usuarioServiceImpl,"formatoCorreoInvalido","Formato de correo invalido");
		ReflectionTestUtils.setField(usuarioServiceImpl,"expRegularPassWord","^[A-Z]{1}[a-z]*\\d{2}$");
		ReflectionTestUtils.setField(usuarioServiceImpl,"formatoPasswordInvalido","Formato de password invalido");
		ReflectionTestUtils.setField(usuarioServiceImpl,"correoYaRegistrado","Correo ya registrado");
		
		String cityCodeMock1 = "1";
		String countryCodeMock1 = "56";
		String numberMock1 = "111111111";
		
		String cityCodeMock2 = "2";
		String countryCodeMock2 = "57";
		String numberMock2 = "22222222";
		
		TelefonoDto telefonoDtoMock1 = new TelefonoDto();
		telefonoDtoMock1.setCitycode(cityCodeMock1);
		telefonoDtoMock1.setCountrycode(countryCodeMock1);
		telefonoDtoMock1.setNumber(numberMock1);
		
		TelefonoDto telefonoDtoMock2 = new TelefonoDto();
		telefonoDtoMock2.setCitycode(cityCodeMock2);
		telefonoDtoMock2.setCountrycode(countryCodeMock2);
		telefonoDtoMock2.setNumber(numberMock2);
		
		List<TelefonoDto> phones = new ArrayList<>();
		phones.add(telefonoDtoMock1);
		phones.add(telefonoDtoMock2);
		
		String email = "algo@gmail.com";
		String name = "userTest";
		String password = "Aaaabbbb22";
		
		UsuarioRequestDto request = new UsuarioRequestDto();
		request.setEmail(email);
		request.setName(name);
		request.setPassword(password);
		request.setPhones(phones);
		
		Telefono telMock1 = new Telefono();
		telMock1.setCitycode(cityCodeMock1);
		telMock1.setCountrycode(countryCodeMock1);
		telMock1.setNumber(numberMock1);
		
		Telefono telMock2 = new Telefono();
		telMock2.setCitycode(cityCodeMock2);
		telMock2.setCountrycode(countryCodeMock2);
		telMock2.setNumber(numberMock2);
		
		List<Telefono> listaTelefono = new ArrayList<>();
		listaTelefono.add(telMock1);
		listaTelefono.add(telMock2);
		
		Usuario usrMock1 = new Usuario();
		UUID uuid = UUID.fromString("1e958bf5-c4b9-47ef-8a96-1ec95570501f");
		usrMock1.setId(uuid);
		usrMock1.setName(request.getName());
		usrMock1.setPassword(request.getPassword());
		Set<Telefono> targetSetMock1 = new HashSet<>(listaTelefono);
		usrMock1.setTelefonos(targetSetMock1);
		usrMock1.setEmail(request.getEmail());
		
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
		String dateInString = "7-Jun-2021";
		Date date = formatter.parse(dateInString);
		
		usrMock1.setCreated(date);
		usrMock1.setLastLogin(date);
		usrMock1.setModified(date);
		
		Mockito.when(iUsuarioRepository.findAll()).thenReturn(new ArrayList<>());
		Mockito.when(iUsuarioRepository.save(Mockito.any())).thenReturn(usrMock1);
		Mockito.when(jwtTokenUtil.isTokenExpired(Mockito.any())).thenReturn(false);
		
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJVTk8iLCJleHAiOjE2MjYxMDk1MDUsImlhdCI6MTYyNjEwOTQwNX0.Pr34kjpGYJzFUeOwabjv2eq_qeKZzB-a5fVtrD8h_GfAdFeYhZZ2ZUNsDzHgV5OHTNkI6QCsfravqdqYH0ttXg";
		
		UsuarioResponseDto resp = usuarioServiceImpl.save(request, token);
		assertNotNull(resp);
		assertEquals(date, resp.getCreated(), "La fecha de creacion no corresponde con la esperada");
		assertEquals(date, resp.getLastLogin(), "La fecha de last login no corresponde con la esperada");
		assertEquals(date, resp.getModified(), "La fecha de modificacion no corresponde con la esperada");
		assertEquals(uuid, resp.getId(), "El UUID no corresponde con el esperado");
		assertEquals(usrMock1, resp.getUsuario(), "El usuario no corresponde con el esperado");
		assertEquals(token, resp.getToken(), "El Token no corresponde con el esperado");
		assertEquals(true, resp.isActive(), "El is active no corresponde con el esperado");
	}
	
	@Test
	public void validaFormatoDatosEntradaCorreoTestNok() {
		
		ReflectionTestUtils.setField(usuarioServiceImpl,"expRegularCorreo","^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
		ReflectionTestUtils.setField(usuarioServiceImpl,"formatoCorreoInvalido","Formato de correo invalido");
		
		UsuarioRequestDto request = new UsuarioRequestDto();
		request.setEmail("algo..@gmail.com");
		UsuarioResponseDto response = new UsuarioResponseDto();
		
		boolean isOk = usuarioServiceImpl.validaFormatoDatosEntrada(request, response);
		assertEquals(false, isOk);
	}
	
	@Test
	public void validaFormatoDatosEntradaPasswordTestNok() {
		
		ReflectionTestUtils.setField(usuarioServiceImpl,"expRegularCorreo","^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");
		ReflectionTestUtils.setField(usuarioServiceImpl,"formatoCorreoInvalido","Formato de correo invalido");
		ReflectionTestUtils.setField(usuarioServiceImpl,"expRegularPassWord","^[A-Z]{1}[a-z]*\\d{2}$");
		ReflectionTestUtils.setField(usuarioServiceImpl,"formatoPasswordInvalido","Formato de password invalido");
		
		UsuarioRequestDto request = new UsuarioRequestDto();
		request.setEmail("algo@gmail.com");
		request.setPassword("hfdjjhf562dhff_khkjh");
		UsuarioResponseDto response = new UsuarioResponseDto();
		
		boolean isOk = usuarioServiceImpl.validaFormatoDatosEntrada(request, response);
		assertEquals(false, isOk);
	}
	
	@Test
	public void validaExistenciaCorreoTestOk() {
		
		String correo = "algo@gmail.com";
		
		Usuario usrMock1 = new Usuario();
		UUID uuid = UUID.fromString("1e958bf5-c4b9-47ef-8a96-1ec95570501f");
		usrMock1.setId(uuid);
		usrMock1.setEmail(correo);
		
		List<Usuario> listaUsuarios = new ArrayList<>();
		listaUsuarios.add(usrMock1);
		
		Mockito.when(iUsuarioRepository.findAll()).thenReturn(listaUsuarios);
		boolean isOk = usuarioServiceImpl.validaExistenciaCorreo(correo);
		assertEquals(true, isOk);
		
	}
	
	@Test
	public void validaExistenciaCorreoTestNoOk() {
		
		String correo = "algo@gmail.com";
		
		Usuario usrMock1 = new Usuario();
		UUID uuid = UUID.fromString("1e958bf5-c4b9-47ef-8a96-1ec95570501f");
		usrMock1.setId(uuid);
		usrMock1.setEmail("juan@gmail.com");
		
		List<Usuario> listaUsuarios = new ArrayList<>();
		listaUsuarios.add(usrMock1);
		
		Mockito.when(iUsuarioRepository.findAll()).thenReturn(listaUsuarios);
		boolean isOk = usuarioServiceImpl.validaExistenciaCorreo(correo);
		assertEquals(false, isOk);
		
	}

}
